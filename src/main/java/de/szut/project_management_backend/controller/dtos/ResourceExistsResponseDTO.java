package de.szut.project_management_backend.controller.dtos;

import lombok.Getter;

@Getter
public class ResourceExistsResponseDTO {
    private boolean exists;
}
