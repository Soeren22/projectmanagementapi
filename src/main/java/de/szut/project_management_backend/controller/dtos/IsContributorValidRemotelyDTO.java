package de.szut.project_management_backend.controller.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class IsContributorValidRemotelyDTO {
    private Long contributorId;
    private boolean knownRemotly;
    private boolean qualifcationKnown;
}
