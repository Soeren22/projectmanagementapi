package de.szut.project_management_backend.controller.dtos;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ProjectmembershipDetails {
    private Long projectId;
    private String projectname;
    private LocalDate start;
    private LocalDate end;
    private String projectRole;
}
