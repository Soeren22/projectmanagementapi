package de.szut.project_management_backend.controller.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ProjectRequestDTO {

    @NotNull
    @NotEmpty
    @Length(max = 100, message = "Must not exceed 100 characters")
    private String name;

    @NotNull
    private Long internalPersonResponsibleId;

    @NotNull
    private Long customerId;

    @Length(max = 100, message = "Must not exceed 100 characters")
    private String externalCoordinator;

    @Length(max = 60000, message = "Allowed length exceeded")
    private String comment;

    @NotNull
    @DateTimeFormat
    private LocalDate plannedEnd;

    @DateTimeFormat
    private LocalDate start;

    @DateTimeFormat
    private LocalDate end;
}
