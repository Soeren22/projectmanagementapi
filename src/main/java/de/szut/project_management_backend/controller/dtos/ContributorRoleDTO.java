package de.szut.project_management_backend.controller.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ContributorRoleDTO {
    private Long ContributorId;
    private String role;
}
