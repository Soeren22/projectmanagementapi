package de.szut.project_management_backend.controller.dtos;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter@NoArgsConstructor
public class ContributorProjectDetailsDTO {
    private Long contributorId;
    private Set<ProjectmembershipDetails> projectMemberships = new HashSet<>();
}
