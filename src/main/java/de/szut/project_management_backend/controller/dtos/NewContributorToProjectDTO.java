package de.szut.project_management_backend.controller.dtos;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class NewContributorToProjectDTO {
    @NotNull
    private Long contributorId;

    @NotNull
    private String role;
}
