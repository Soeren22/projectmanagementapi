package de.szut.project_management_backend.controller.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
public class ProjectContributorsDTO {
    private Long projectId;
    private String name;
    Set<ContributorRoleDTO> contributers = new HashSet<>();

    public void addContributor(ContributorRoleDTO dto){
        this.contributers.add(dto);
    }
}
