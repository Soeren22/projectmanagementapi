package de.szut.project_management_backend.controller.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ProjectResponseDTO {
    private Long projectId;
    private String name;
    private Long internalPersonResponsibleId;
    private Long customerId;
    private String externalCoordinator;
    private String comment;
    private LocalDate plannedEnd;
    private LocalDate start;
    private LocalDate end;
}
