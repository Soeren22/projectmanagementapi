package de.szut.project_management_backend.controller;
import de.szut.project_management_backend.controller.dtos.*;
import de.szut.project_management_backend.controller.exceptionHandling.IsNotMemberOfProjectException;
import de.szut.project_management_backend.controller.exceptionHandling.NoQualificationException;
import de.szut.project_management_backend.controller.exceptionHandling.ResourceNotFoundException;
import de.szut.project_management_backend.controller.exceptionHandling.StartOrEndDateMissingException;
import de.szut.project_management_backend.controller.service.model.ProjectEntity;
import de.szut.project_management_backend.controller.service.ContributorService;
import de.szut.project_management_backend.controller.service.MappingService;
import de.szut.project_management_backend.controller.service.ProjectService;
import de.szut.project_management_backend.controller.service.RemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("projectmanagement_api/projects")
public class ProjectAPIController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ContributorService contributorService;

    @Autowired
    private MappingService mappingService;

    @Autowired
    private RemoteService remoteService;

    /**
     * Get list of all projects.
     *
     * @return response entity with projects as json array and http status code
     */
    @GetMapping
    public ResponseEntity<List<ProjectResponseDTO>> getAllProjects() {
        List<ProjectEntity> projectList = this.projectService.getAllProjects();
        if (projectList.isEmpty()) {
            throw new ResourceNotFoundException("No projects found.");
        }
        List<ProjectResponseDTO> response = new LinkedList<>();
        for (ProjectEntity p: projectList) {
            response.add(this.mappingService.mapProjectEntityToProjectResponseDTO(p));
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Get single project by projectId
     *
     * @param projectId project ID
     * @return response entity with project as json array and http status code
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<ProjectResponseDTO> getProjectById(@PathVariable(value = "id") Long projectId) {
        ProjectEntity entity = projectService.getProjectById(projectId);
        if(entity== null){
            throw new ResourceNotFoundException("Project not found on id = "+projectId);
        }
        ProjectResponseDTO response = this.mappingService.mapProjectEntityToProjectResponseDTO(entity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Get single project by projectId
     *
     * @param projectId project ID
     * @return response entity with project as json array and http status code
     */
    @GetMapping(value = "/{id}/contributers")
    public ResponseEntity<ProjectContributorsDTO> getAllContributersByProjectId(@PathVariable(value = "id") Long projectId) {
        ProjectEntity entity = projectService.getProjectById(projectId);
        if(entity== null){
            throw new ResourceNotFoundException("Project not found on id = "+projectId);
        }
        ProjectContributorsDTO response = this.mappingService.mapProjectEntityToProjectContributorsDTO(entity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Create a new project
     *
     * @param project   NewProjectRequestDTO object
     * @return response entity with project as a json object and http status code
     */
    @PostMapping
    public ResponseEntity<ProjectResponseDTO> createProject(@Valid @RequestBody ProjectRequestDTO project) {
        ProjectEntity newProject = this.mappingService.mapProjectRequestDTOTOEntity(project);
        Long contributorId = newProject.getInternalPersonResponsibleId();
        if(!this.remoteService.isContributorKnownRemotely(contributorId)){
            throw new ResourceNotFoundException("Contributor with id = "+ contributorId + " is not known.");
        }
        newProject = projectService.createProject(newProject);
        ProjectResponseDTO response = this.mappingService.mapProjectEntityToProjectResponseDTO(newProject);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /**
     * Update existing project
     * @param id project ID
     * @param updatedProject   UpdateProjectRequest object
     * @return response entity with ProjectResponseDTO as json object and http status code
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<ProjectResponseDTO> updateProject(@PathVariable Long id, @Valid @RequestBody ProjectRequestDTO updatedProject) {
        if(!this.projectService.existsById(id)){
            throw new ResourceNotFoundException("Project not found on id="+id);
        }
        Long contributorId = updatedProject.getInternalPersonResponsibleId();
        if(!this.remoteService.isContributorKnownRemotely(contributorId)){
            throw new ResourceNotFoundException("Contributor with id = "+ contributorId + " is not known.");
        }
        ProjectEntity projectEntity = this.mappingService.mapProjectRequestDTOTOEntity(updatedProject);
        projectEntity.setProjectId(id);
        projectEntity = this.projectService.updateProject(projectEntity);
        ProjectResponseDTO response = this.mappingService.mapProjectEntityToProjectResponseDTO(projectEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Delete a project by projectID
     *
     * @param projectId project ID
     */
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteProject(@PathVariable(value = "id") Long projectId) {
        if(!this.projectService.existsById(projectId)){
            throw new ResourceNotFoundException("Project not found on id="+projectId);
        }
        this.projectService.deleteProject(projectId);
    }

    /**
     * adds a contributor to a given project
     *
     * @param request   NewContributorToProjectDTO object
     * @return response entity with project as a json object and http status code
     */
    @PostMapping("/{id}/contributors")
    public ResponseEntity<ProjectContributorsDTO> addContributorToProject(@PathVariable Long id,  @Valid @RequestBody NewContributorToProjectDTO request){
        if(!this.projectService.existsById(id)){
            throw new ResourceNotFoundException("Project not found on id="+id);
        }
        if(!this.projectService.hasProjectStartAndEnd(id)){
            throw new StartOrEndDateMissingException("Start- or Enddate is missing on Project id = "+ id);
        }
        IsContributorValidRemotelyDTO dto = this.remoteService.getContributorValidRemotelyDTO(request.getContributorId(), request.getRole());
        if(!dto.isKnownRemotly()){
            throw new ResourceNotFoundException("Contributor with id = "+ request.getContributorId() + " is not known.");
        }
        if(!dto.isQualifcationKnown()){
            throw new NoQualificationException("Contributor does not have this qualification!");
        }

        ProjectEntity project = this.projectService.addContributorToProject(id, request.getContributorId(), request.getRole());
        ProjectContributorsDTO response = this.mappingService.mapProjectEntityToProjectContributorsDTO(project);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /**
     * deletes a contributor from project by projectID
     *
     * @param projectId  project ID
     * @param contributorId  contributor Id
     * @return response entity with short project data and projectMembers as a json object and http status code
     */
    @DeleteMapping("/{projectId}/contributors/{contributorId}")
    public ResponseEntity<ProjectContributorsDTO> deleteContributorFromProject(@PathVariable Long projectId, @PathVariable Long contributorId) {
        if(!this.projectService.existsById(projectId)){
            throw new ResourceNotFoundException("Project not found on id="+projectId);
        }
        if(!this.projectService.isMemberOfProject(projectId, contributorId )){
            throw new IsNotMemberOfProjectException("Contributor with id = "+ contributorId + " is not member of project on id = "+ projectId);
        }
        ProjectEntity project = this.projectService.deleteContributorFromProject(projectId, contributorId);
        ProjectContributorsDTO response = this.mappingService.mapProjectEntityToProjectContributorsDTO(project);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}