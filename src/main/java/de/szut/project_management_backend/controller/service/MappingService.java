package de.szut.project_management_backend.controller.service;

import de.szut.project_management_backend.controller.dtos.*;
import de.szut.project_management_backend.controller.service.model.ContributorEntity;
import de.szut.project_management_backend.controller.service.model.ProjectMembersEntity;
import de.szut.project_management_backend.controller.service.model.ProjectEntity;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class MappingService {
    public ProjectResponseDTO mapProjectEntityToProjectResponseDTO(ProjectEntity entity){
        ProjectResponseDTO dto = new ProjectResponseDTO();
        dto.setProjectId(entity.getProjectId());
        dto.setName(entity.getName());
        dto.setInternalPersonResponsibleId(entity.getInternalPersonResponsibleId());
        dto.setCustomerId(entity.getCustomerId());
        dto.setExternalCoordinator(entity.getExternalCoordinator());
        dto.setComment(entity.getComment());
        dto.setPlannedEnd(entity.getPlannedEnd());
        dto.setStart(entity.getStart());
        dto.setEnd(entity.getEnd());
        return dto;
    }

    public ProjectEntity mapProjectRequestDTOTOEntity(ProjectRequestDTO dto){
        ProjectEntity entity = new ProjectEntity();
        entity.setName(dto.getName());
        entity.setInternalPersonResponsibleId(dto.getInternalPersonResponsibleId());
        entity.setCustomerId(dto.getCustomerId());
        entity.setExternalCoordinator(dto.getExternalCoordinator());
        entity.setComment(dto.getComment());
        entity.setPlannedEnd(dto.getPlannedEnd());
        entity.setStart(dto.getStart());
        entity.setEnd(dto.getEnd());
        return entity;
    }

    public ProjectContributorsDTO mapProjectEntityToProjectContributorsDTO(ProjectEntity entity){
        ProjectContributorsDTO dto = new ProjectContributorsDTO();
        dto.setProjectId(entity.getProjectId());
        dto.setName(entity.getName());
        for (ProjectMembersEntity membersEntity: entity.getContributers()) {
            ContributorRoleDTO contributor = new ContributorRoleDTO();
            contributor.setContributorId(membersEntity.getContributor().getContributorId());
            contributor.setRole(membersEntity.getProjectRole());
            dto.addContributor(contributor);
        }
        return dto;
    }

    public ProjectContributerDetailsDTO mapProjectEntityToProjectContributerDetailsDTO(ContributorEntity contributor, ProjectEntity entity){
        ProjectContributerDetailsDTO dto = new ProjectContributerDetailsDTO();
        dto.setProjectId(entity.getProjectId());
        dto.setContributorId(contributor.getContributorId());
        dto.setProjectRole(entity.getProjectContributorEntityByContributor(contributor).getProjectRole().toString());
        return dto;
    }

    public ContributorProjectDetailsDTO mapProjectMembersEntityToContributorProjectDetailsDTO() {
        return mapProjectMembersEntityToContributorProjectDetailsDTO();
    }

    public ContributorProjectDetailsDTO mapProjectMembersEntityToContributorProjectDetailsDTO(Long contributerId, Set<ProjectMembersEntity> memberships){
        ContributorProjectDetailsDTO dto = new ContributorProjectDetailsDTO();
        dto.setContributorId(contributerId);
        for (ProjectMembersEntity member: memberships) {
            ProjectmembershipDetails details = new ProjectmembershipDetails();
            details.setProjectId(member.getProject().getProjectId());
            details.setProjectname(member.getProject().getName());
            details.setStart(member.getProject().getStart());
            details.setEnd(member.getProject().getEnd());
            details.setProjectRole(member.getProjectRole());
            dto.getProjectMemberships().add(details);
        }
        return dto;
    }
}
