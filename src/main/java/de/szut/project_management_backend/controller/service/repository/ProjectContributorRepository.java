package de.szut.project_management_backend.controller.service.repository;

import de.szut.project_management_backend.controller.service.model.ContributorEntity;
import de.szut.project_management_backend.controller.service.model.ProjectEntity;
import de.szut.project_management_backend.controller.service.model.ProjectMembersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface ProjectContributorRepository extends JpaRepository<ProjectMembersEntity, Long> {
    List<ProjectMembersEntity> findByContributor(ContributorEntity contributor);
    ProjectMembersEntity findByProjectAndContributor(ProjectEntity project, ContributorEntity contributor);
}
