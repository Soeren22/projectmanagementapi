package de.szut.project_management_backend.controller.service;

import de.szut.project_management_backend.controller.service.model.ContributorEntity;
import de.szut.project_management_backend.controller.service.repository.ContributorRepository;
import org.springframework.stereotype.Service;
import java.util.Optional;

/**
 * Collection of service methods for handling contributers
 */
@Service
public class ContributorService {

    private final ContributorRepository contributorRepository;

    public ContributorService(ContributorRepository contributorRepository) {
        this.contributorRepository = contributorRepository;
    }

    /**
     * Get user from database by contributor ID
     *
     * @param cId contributor ID
     * @return ContributorEntity
     */
    public ContributorEntity getContributorById(Long cId) {
        Optional<ContributorEntity> contributorOpt = this.contributorRepository.findById(cId);
        if (contributorOpt.isPresent()) {
            return contributorOpt.get();
        }
        return null;
    }

    /**
     * Determine if contributor is known in pms database
     *
     * @param id Long
     * @return boolean
     */
    private boolean isContributorKnownLocally(Long id) {
        Optional<ContributorEntity> foundContributor = this.contributorRepository.findById(id);
        return foundContributor.isPresent();
    }

    public void saveContributorIfNotKnownLocally(Long contributorId){
        if(!isContributorKnownLocally(contributorId)){
            save(new ContributorEntity(contributorId));
        }
    }

    /**
     * Save ContributorEntity object to database
     *
     * @param contributor ContributorEntity
     */
    public void save(ContributorEntity contributor) {
        this.contributorRepository.save(contributor);
    }
}
