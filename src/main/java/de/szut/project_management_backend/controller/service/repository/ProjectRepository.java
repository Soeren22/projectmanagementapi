package de.szut.project_management_backend.controller.service.repository;

import de.szut.project_management_backend.controller.service.model.ProjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<ProjectEntity, Long> {
}
