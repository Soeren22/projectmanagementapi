package de.szut.project_management_backend.controller.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "ProjectMembers")
public class ProjectMembersEntity {

    @EmbeddedId
    private ProjectContributorKey id;

    @ManyToOne
    @MapsId("projectId")
    @JoinColumn(name = "project_id")
    private ProjectEntity project;

    @ManyToOne
    @MapsId("contributorId")
    @JoinColumn(name = "contributor_id")
    private ContributorEntity contributor;

    @Column(name = "project_role")
    private String projectRole;
}
