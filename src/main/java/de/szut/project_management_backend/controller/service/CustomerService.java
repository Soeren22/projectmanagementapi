package de.szut.project_management_backend.controller.service;

import de.szut.project_management_backend.controller.exceptionHandling.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Collection of service methods concerning customers
 */
@Service
public class CustomerService {

    private final RemoteService remoteService;

    @Autowired
    public CustomerService(RemoteService remoteService) {
        this.remoteService = remoteService;
    }

    /**
     * Call service to determine if customer ID is known by remote customer service
     *
     * @param customerId customer ID
     * @return boolean
     */
    private boolean isCustomerKnownRemotely(Long customerId) {
        return remoteService.isCustomerKnownRemotely(customerId);
    }

    public void assertThatCustomerIsKnownRemotely(Long id){
        if(!isCustomerKnownRemotely(id)){
            throw new ResourceNotFoundException("No such customer on id = "+ id);
        }
    }
}
