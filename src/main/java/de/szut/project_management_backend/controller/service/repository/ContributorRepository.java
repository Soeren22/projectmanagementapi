package de.szut.project_management_backend.controller.service.repository;

import de.szut.project_management_backend.controller.service.model.ContributorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContributorRepository extends JpaRepository<ContributorEntity, Long> {
}
