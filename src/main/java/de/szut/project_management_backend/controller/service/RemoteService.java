package de.szut.project_management_backend.controller.service;

import de.szut.project_management_backend.controller.dtos.*;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RemoteService {

    private RestTemplate restTemplate;
    private String employee_url = "http://localhost:8080/employee_api/employees/";
    private String customer_url = "";

    public RemoteService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public boolean isContributorKnownRemotely(Long employeeId) {
       try{
           ContributorNameAndSkillDataResponseDTO response= this.restTemplate.getForObject(employee_url + employeeId + "/qualifications", ContributorNameAndSkillDataResponseDTO.class);
           return true;
       }
       catch(Exception ex){
           return false;
       }
    }

    public IsContributorValidRemotelyDTO getContributorValidRemotelyDTO(Long contributorId, String qualification) {
        IsContributorValidRemotelyDTO dto = new IsContributorValidRemotelyDTO();
        dto.setContributorId(contributorId);
        try{
            ContributorNameAndSkillDataResponseDTO response= this.restTemplate.getForObject(employee_url + contributorId + "/qualifications", ContributorNameAndSkillDataResponseDTO.class);
            dto.setKnownRemotly(true);
            for (QualificationDTO qualificationDTO: response.getSkillSet()) {
                if(qualificationDTO.getDesignation().equals(qualification)){
                    dto.setQualifcationKnown(true);
                    return dto;
                }
            }
            return dto;
        }
        catch(Exception ex){
            dto.setKnownRemotly(false);
            return dto;
        }
    }


       // FIXME: Dummy method

       /**
        * Determine if given customer ID is known by remote CustomerService
        *
        * @param customerId customer ID
        * @return boolean
        */
    public boolean isCustomerKnownRemotely(Long customerId) {
        return true;
    }
}
