package de.szut.project_management_backend.controller.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "contributor")
public class ContributorEntity {

    @Id
    @Column(name = "contributorId", nullable = false, unique = true)
    private Long contributorId;

    @OneToMany(mappedBy = "contributor", fetch = FetchType.EAGER)
    private Set<ProjectMembersEntity> projectMemberships;

    public void addProjectMembership(ProjectMembersEntity membership){
        this.projectMemberships.add(membership);
    }

    public void removeProjectMembership(ProjectEntity project){
        for (ProjectMembersEntity membership: this.getProjectMemberships()) {
            if(membership.getProject().getProjectId()==project.getProjectId()){
                this.projectMemberships.remove(membership);
            }
        }
    }
    public ContributorEntity(Long id){
        this.contributorId = id;
        this.projectMemberships = new HashSet<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContributorEntity contributor = (ContributorEntity) o;
        return Objects.equals(this.contributorId, contributor.getContributorId());
    }
}
