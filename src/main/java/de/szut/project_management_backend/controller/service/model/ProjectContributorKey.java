package de.szut.project_management_backend.controller.service.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ProjectContributorKey implements Serializable{
    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "contributor_id")
    private Long contributorId;

    public ProjectContributorKey() {
    }

    public ProjectContributorKey(Long projectId, Long contributorId) {
            this.projectId = projectId;
            this.contributorId = contributorId;
    }

    public Long getProjectId() {
            return projectId;
        }

    public void setProjectId(Long projectId) {
            this.projectId = projectId;
    }

    public Long getContributorId() {
            return contributorId;
    }

    public void setContributorId(Long contributorId) {
            this.contributorId = contributorId;
    }

    @Override
    public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
        ProjectContributorKey that = (ProjectContributorKey) o;
         return projectId.equals(that.projectId) &&
                 contributorId.equals(that.contributorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectId, contributorId);
    }
}
