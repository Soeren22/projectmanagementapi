package de.szut.project_management_backend.controller.service;

import de.szut.project_management_backend.controller.exceptionHandling.ContributorIsNotAvailableException;
import de.szut.project_management_backend.controller.exceptionHandling.IsMemberOfProjectException;
import de.szut.project_management_backend.controller.service.model.ContributorEntity;
import de.szut.project_management_backend.controller.service.model.ProjectContributorKey;
import de.szut.project_management_backend.controller.service.model.ProjectMembersEntity;
import de.szut.project_management_backend.controller.service.model.ProjectEntity;
import de.szut.project_management_backend.controller.service.repository.ProjectContributorRepository;
import de.szut.project_management_backend.controller.service.repository.ProjectRepository;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.*;

/**
 * Collection of service methods for handling projects
 */
@Service
public class ProjectService {
    private final ProjectRepository projectRepository;
    private final ContributorService contributorService;
    private final CustomerService customerService;
    private final ProjectContributorRepository projectContributorRepository;

    public ProjectService(ProjectRepository projectRepository, ContributorService contributorService, CustomerService customerService, ProjectContributorRepository projectContributorRepository) {
        this.projectRepository = projectRepository;
        this.contributorService = contributorService;
        this.customerService = customerService;
        this.projectContributorRepository = projectContributorRepository;
    }

    /**
     * Retrieve a list of all available projects)
     *
     * @return List<ProjectEntity>
     */
    public List<ProjectEntity> getAllProjects() {
        return projectRepository.findAll();
    }

    /**
     * Retrieve a single project from database by ID
     *
     * @param projectId project ID
     * @return ProjectEntity
     */
    public ProjectEntity getProjectById(Long projectId) {
        Optional<ProjectEntity> projectOpt = projectRepository.findById(projectId);
        if (projectOpt.isEmpty()) {
            return null;
        }
        return projectOpt.get();
    }

    /**
     * Create a new project
     *
     * @param newProject   NewProjectRequestDTO object
     */
    public ProjectEntity createProject(ProjectEntity newProject) {
        Long cId = newProject.getInternalPersonResponsibleId();
        this.contributorService.saveContributorIfNotKnownLocally(cId);
        return this.projectRepository.save(newProject);
    }

    public boolean existsById(Long id){
        return this.projectRepository.existsById(id);
    }

    public ProjectEntity addContributorToProject(Long projectId, Long contributorId, String role){
       ProjectEntity project = getProjectById(projectId);
       this.contributorService.saveContributorIfNotKnownLocally(contributorId);
       ContributorEntity newContributor = this.contributorService.getContributorById(contributorId);
       if(project.isMember(newContributor)){
            throw new IsMemberOfProjectException("This contributor ist already member of this project.");
       }
       if(!isContributorAvailable(project.getStart(), project.getPlannedEnd(), newContributor)){
            throw new ContributorIsNotAvailableException("ContributorEntity with id = " + contributorId + " is not available for that project!");
       }
       ProjectMembersEntity newMembership = new ProjectMembersEntity();
       ProjectContributorKey key = new ProjectContributorKey(projectId, contributorId);
       newMembership.setId(key);
       newMembership.setProject(project);
       newMembership.setContributor(newContributor);
       newMembership.setProjectRole(role);
       project.addContributorToProject(newMembership);
       return this.projectRepository.save(project);
     }

    private boolean isContributorAvailable(LocalDate start, LocalDate end, ContributorEntity contributor){
        List<ProjectMembersEntity> list = this.projectContributorRepository.findByContributor(contributor);
        if(list.isEmpty()){
            return true;
        }
        LocalDate startOtherProject;
        LocalDate endOtherProject;
        for (ProjectMembersEntity pc: list) {
            startOtherProject = pc.getProject().getStart();
            endOtherProject = pc.getProject().getPlannedEnd();
            if(projectIsBeforeOtherProject(end, startOtherProject) || projectIsAfterOtherProject(start, endOtherProject)){
                return true;
            }
        }
        return false;
    }

    private boolean projectIsBeforeOtherProject(LocalDate end, LocalDate startOtherProject){
        return end.isBefore(startOtherProject);
    }

    private boolean projectIsAfterOtherProject(LocalDate start, LocalDate endOtherProject){
        return start.isAfter(endOtherProject);
    }

    /**
     * Update an existing project
     *
     * @param updateProject ProjectEntity object
     */
    public ProjectEntity updateProject(ProjectEntity updateProject) {
        ProjectEntity project = getProjectById(updateProject.getProjectId());
        project.setName(updateProject.getName());
        project.setInternalPersonResponsibleId(updateProject.getInternalPersonResponsibleId());
        project.setCustomerId(updateProject.getCustomerId());
        project.setExternalCoordinator(updateProject.getExternalCoordinator());
        project.setComment(updateProject.getComment());
        project.setPlannedEnd(updateProject.getPlannedEnd());
        project.setStart(updateProject.getStart());
        project.setEnd(updateProject.getEnd());
        return projectRepository.save(project);
    }

    /**
     * Delete a project by ID
     *
     * @param projectId project ID
     */
    public void deleteProject(Long projectId) {
        ProjectEntity project = getProjectById(projectId);
        this.projectRepository.delete(project);
    }

    public ProjectEntity deleteContributorFromProject(Long projectId, Long contributorId){
        ProjectEntity project = this.getProjectById(projectId);
        ContributorEntity contributor= this.contributorService.getContributorById(contributorId);
        project.removeContributorFromProject(contributor);
        contributor.removeProjectMembership(project);
        project= this.projectRepository.save(project);
        ProjectMembersEntity membership = this.projectContributorRepository.findByProjectAndContributor(project, contributor);
        this.projectContributorRepository.delete(membership);
        return this.getProjectById(projectId);
    }

    public boolean isMemberOfProject(Long project_Id, Long contributor_Id){
        ProjectEntity project = this.getProjectById(project_Id);
        ContributorEntity contributor = this.contributorService.getContributorById(contributor_Id);
        if (contributor == null){
            return false;
        }
        return project.isMember(contributor);
    }

    public boolean hasProjectStartAndEnd(Long projectId){
        ProjectEntity project = getProjectById(projectId);
        return project.hasStartAndEndDate();
    }
}
