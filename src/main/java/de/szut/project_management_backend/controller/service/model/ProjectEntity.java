package de.szut.project_management_backend.controller.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "project")
public class ProjectEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id", nullable = false, unique = true)
    private Long projectId;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "personResponsible_id")
    private Long internalPersonResponsibleId;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "external_coordinator")
    private String externalCoordinator;

    @Column(name = "comment")
    private String comment;

    @Column(name = "planned_end")
    @DateTimeFormat
    private LocalDate plannedEnd;

    @Column(name = "start")
    @DateTimeFormat
    private LocalDate start;

    @Column(name = "end")
    @DateTimeFormat
    private LocalDate end;

    @OneToMany(mappedBy = "project", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<ProjectMembersEntity> contributers = new HashSet<>();

    public void addContributorToProject(ProjectMembersEntity projectContributor) {
        this.contributers.add(projectContributor);
    }

    public void removeContributorFromProject(ContributorEntity contributor){
        Iterator iter = this.contributers.iterator();
        while(iter.hasNext()){
            ProjectMembersEntity membership = (ProjectMembersEntity) iter.next();
            if(membership.getContributor().getContributorId()== contributor.getContributorId()){
                iter.remove();
            }
        }
    }

    /**
     * Determine if a given projectContributor is a member of the project (i.e. is a ProjectMembersEntity)
     *
     * @param projectContributor
     * @return boolean
     */
    public boolean isMember(ContributorEntity projectContributor) {
        if (this.contributers != null) {
            for (ProjectMembersEntity member : this.contributers) {
                if (member.getContributor().equals(projectContributor)) {
                    return true;
                }
            }
        }
        return false;
    }

    public ProjectMembersEntity getProjectContributorEntityByContributor(ContributorEntity contributor){
        for (ProjectMembersEntity pce : this.contributers) {
            if(pce.getContributor().equals(contributor))
                return pce;
        }
        return null;
    }

    public boolean hasStartAndEndDate(){
        if(this.start!=null && this.end!=null)
            return true;
        else
            return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectEntity project = (ProjectEntity) o;
        return Objects.equals(projectId, project.projectId)
                && Objects.equals(name, project.name)
                && Objects.equals(customerId, project.customerId)
                && Objects.equals(externalCoordinator, project.externalCoordinator)
                && Objects.equals(comment, project.comment)
                && Objects.equals(plannedEnd, project.plannedEnd)
                && Objects.equals(start, project.start)
                && Objects.equals(end, project.end)
                && Objects.equals(contributers, project.contributers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                projectId,
                name,
                customerId,
                externalCoordinator,
                comment,
                plannedEnd,
                start,
                end
        );
    }
}
