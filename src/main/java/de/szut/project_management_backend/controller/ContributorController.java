package de.szut.project_management_backend.controller;

import de.szut.project_management_backend.controller.dtos.ContributorProjectDetailsDTO;
import de.szut.project_management_backend.controller.exceptionHandling.ResourceNotFoundException;
import de.szut.project_management_backend.controller.service.model.ContributorEntity;
import de.szut.project_management_backend.controller.service.ContributorService;
import de.szut.project_management_backend.controller.service.MappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("projectmanagement_api/contributers")
public class ContributorController {
    @Autowired
    private ContributorService contributorService;

    @Autowired
    private MappingService mappingService;

    @GetMapping("/{id}/projects")
    public ResponseEntity<ContributorProjectDetailsDTO> getAllProjectsByContributerId(@PathVariable Long id){
        ContributorEntity contributor = this.contributorService.getContributorById(id);
        if(contributor == null){
            throw new ResourceNotFoundException("Contributor not found on id = "+ id);
        }
        ContributorProjectDetailsDTO dto = this.mappingService.mapProjectMembersEntityToContributorProjectDetailsDTO(id, contributor.getProjectMemberships());
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
