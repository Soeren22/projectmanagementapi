package de.szut.project_management_backend.controller.exceptionHandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class IsNotMemberOfProjectException extends RuntimeException {
    public IsNotMemberOfProjectException(String message) {
        super(message);
    }
}
